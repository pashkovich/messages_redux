import {
    SELECT_MESSAGE, REMOVE_LABEL, CLICK_LABEL, DELETE_LABEL
}
from '../constants/Messages'

const initialState = [{
    id: '1',
    title: 'Message 1',
    label: [],
    status: false
}, {
    id: '2',
    title: 'Message 2',
    label: [],
    status: false
}, {
    id: '3',
    title: 'Message 3',
    label: [],
    status: false
}, {
    id: '4',
    title: 'Message 4',
    label: [],
    status: false
} ]


export
default

function messages(state = initialState, action) {

    switch(action.type) {
        case SELECT_MESSAGE:
            return state.map(item =>
                item.id === action.id ?
                { ...item, status: !item.status }
                : item
            )

        case CLICK_LABEL:
            return state.map(item =>
                item.status ?
                { ...item, 
                    label: (item.label.some(el => el.id === action.label.id)) ? [...item.label] : [...item.label, action.label]
                } : item
            );

        case REMOVE_LABEL:
            return [...state].map(el => ({
                    ...el,
                label: el.status ?
                    el.label.filter(item => item.id !== action.id) : el.label
            }))

        case DELETE_LABEL:
            return [...state].map(el => ({
                    ...el,
                label: el.label.filter(item => item.id !== action.id)
            }))


        default:
            return state;
    }

}