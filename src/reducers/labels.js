import {
    ADD_LABEL, DELETE_LABEL, CHANGE_LABEL
}
from '../constants/Messages'


const initialState = [{
    title: 'label 1',
    id: 1,
    status: false
}, {
    title: 'label 2',
    id: 2,
    status: false
}, {
    title: 'label 3',
    id: 3,
    status: false
}, {
    title: 'label 4',
    id: 4,
    status: false
}]


export
default
function labels(state = initialState, action) {
    switch (action.type) {

        case ADD_LABEL:
            return state.some( item => item.title.toLowerCase() == action.title.toLowerCase()) ? state : [...state, {
                title: action.title,
                id: Math.random(),
                active: false
            }]

        case DELETE_LABEL:
            return state.filter(
                label => label.id !== action.id
            )

        case CHANGE_LABEL:
            console.log(action.messages.filter( i => i.status == true).some( i => i.label.some( k => k.id == 1)))
            return state.map( item => (
                { ...item,
                status : action.messages
                .filter( i => i.status == true)
                .some( j => j.label
                    .some( k => k.id == item.id))
                }
            ));
            
        default:
            return state;
    }
}