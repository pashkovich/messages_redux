import React, { PropTypes, Component } from 'react'

export default class Messages extends Component {
  selectCheckBox(message, event) {
    let { selectMessage, changeLabel } = this.props.messagesActions;
    selectMessage(message, event.target.checked);
    setTimeout(() => changeLabel(this.props.messages), 0);
  }
  render() {
    const { messages } = this.props
    return <div className='ib page'>
    {
      messages.map( item => {
        return (
          <div key={item.id}>
            <input type="checkbox" onChange={ ( event ) => this.selectCheckBox( item, event ) } checked={ item.active } />
            <div className="message">{item.title}
              {
                item.label.map( label => {
                  return (
                    <span key={label.id} className="label">{label.title}</span>
                    )
                })
              }
            </div>
          </div>
          )
      })
    }
    </div>
  }
}

Messages.propTypes = {
  messages: PropTypes.array.isRequired
}
