import React, { PropTypes, Component } from 'react'

export default class Labels extends Component {
	constructor(props) {
    super(props);
    this.state = {
      value: ''
    };
  }

  onClickSubmit = ( event ) => {
    let { addLabel } = this.props.labelsActions;
    if ( event.keyCode === 13 ) {
      let { value } = this.state;
      if ( value ) {
        addLabel( value );
        this.setState({
          value: ''
        });
      }  
    }
  }
  onClickRemove = ( id ) => {
    let { removeLabel, changeLabel } = this.props.labelsActions;
    removeLabel(id);
    changeLabel();
  }
  onClickLabel = ( label ) => {
    let { clickLabel, changeLabel } = this.props.labelsActions;
    clickLabel( label );
    changeLabel();
  }
  onClickDelete = ( id ) => {
    let { deleteLabel } = this.props.labelsActions;
    deleteLabel(id);
  }

  _getButtonRemove = item => {
    const style = {display: item.status ? 'inline' : 'none'};
    return <button className="btn" style = { style } onClick={ () => this.onClickRemove( item.id ) }>×</button>
  }

  onChangeInput = ( event ) => 
    this.setState({
      value: event.target.value
    })  
  render() {
    const { labels } = this.props
    return <div className='ib user'>
    		<input placeholder="Input label" className="input" onChange={ this.onChangeInput } onKeyDown={ this.onClickSubmit } value={ this.state.value } />
    		{
    			labels.map( item => {
    				return (
    					<div className="labels" key={item.id}>
    						<span className="btn" onClick={ () => this.onClickLabel( item ) }>{item.title}</span>
                { this._getButtonRemove(item) }
    						<button onClick={ () => this.onClickDelete( item.id ) }>Delete</button>
    					</div>
    					)
    			})
    		}
        </div>
  }
}

Labels.propTypes = {
	labels: PropTypes.array.isRequired
}
