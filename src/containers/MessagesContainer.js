import React, { Component } from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import Messages from '../components/Messages'
import * as messagesActions from '../actions/MessagesActions'

class MessagesContainer extends Component {
  render = () =>
    <Messages { ...this.props } />
}

const mapStateToProps = state => ({
  messages: state.messages
})

const mapDispatchToProps = dispatch => ({
  messagesActions: bindActionCreators(messagesActions, dispatch)
})

export default connect(mapStateToProps, mapDispatchToProps)(MessagesContainer)