import React, { Component } from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import Labels from '../components/Labels'
import * as labelsActions from '../actions/LabelsActions'

class LabelsContainer extends Component {
  render = () => 
    <Labels { ...this.props } />
}

const mapStateToProps = state => ({
  labels: state.labels
})

const mapDispatchToProps = dispatch => ({
  labelsActions: bindActionCreators( labelsActions, dispatch )
})

export default connect( mapStateToProps, mapDispatchToProps )( LabelsContainer )