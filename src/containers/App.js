import React, { Component } from 'react'
import { connect } from 'react-redux'
import Labels from '../containers/LabelsContainer'
import Messages from '../containers/MessagesContainer'

class App extends Component {
  render() {

    return <div className='row'>
      <Messages />
      <Labels />
    </div>
  }
}

export default connect()(App)
