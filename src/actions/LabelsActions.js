import {
    ADD_LABEL,
    REMOVE_LABEL,
    DELETE_LABEL,
    CLICK_LABEL,
    CHANGE_LABEL
}
from '../constants/Messages'

export
const addLabel = (title) => ({
    type: ADD_LABEL,
    title
})

export
const deleteLabel = (id) => ({
    type: DELETE_LABEL,
    id
})

export
const removeLabel = (id) => ({
    type: REMOVE_LABEL,
    id
})

export
const clickLabel = (label) => ({
    type: CLICK_LABEL,
    label
})
export
const changeLabel =() => 
    (dispatch, getState) => 
        dispatch({
            type: CHANGE_LABEL,
            messages: getState().messages
        })