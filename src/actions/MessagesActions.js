import {
    SET_YEAR, SELECT_MESSAGE, CHANGE_LABEL
}
from '../constants/Messages'

export
function setYear(year) {

    return {
        type: SET_YEAR,
        payload: year
    }

}
export
function selectMessage(message, isChecked) {
    return {
        type: SELECT_MESSAGE,
        labels: message.labels,
        id: message.id,
        status: isChecked
    }
}
export
function changeLabel(messages) {
    return {
        type: CHANGE_LABEL,
        messages: messages
    }
}